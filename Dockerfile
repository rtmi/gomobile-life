FROM patterns/android

WORKDIR /go/src
COPY . /go/src/app

RUN gomobile build -target=android app

