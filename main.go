// +build darwin linux windows

// Modified as described in the game of life tutorial
// see https://kylewbanks.com/blog/tutorial-opengl-with-golang-part-2-drawing-the-game-board
//
package main

import (
	"encoding/binary"
	"log"
	"math/rand"
	"time"

	"golang.org/x/mobile/app"
	"golang.org/x/mobile/event/lifecycle"
	"golang.org/x/mobile/event/paint"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
	"golang.org/x/mobile/exp/app/debug"
	"golang.org/x/mobile/exp/f32"
	"golang.org/x/mobile/exp/gl/glutil"
	"golang.org/x/mobile/gl"
)

// Unit element composing the grid
// where drawable is the square Vertex Array Object
// and the x,y is the position inside the grid
type cell struct {
	drawable    gl.Buffer
	alive       bool
	aliveNext   bool
	aliveFrames int
	rval        float32
	gval        float32
	bval        float32
	x           int
	y           int
}

var (
	images   *glutil.Images
	fps      *debug.FPS
	program  gl.Program
	position gl.Attrib
	color    gl.Uniform
	cells    [][]*cell

	touchX float32
	touchY float32
)

func main() {
	app.Main(func(a app.App) {
		var glctx gl.Context
		var sz size.Event

		for e := range a.Events() {
			switch e := a.Filter(e).(type) {
			case lifecycle.Event:
				switch e.Crosses(lifecycle.StageVisible) {
				case lifecycle.CrossOn:
					glctx, _ = e.DrawContext.(gl.Context)
					onStart(glctx)
					cells = makeCells(glctx)
					a.Send(paint.Event{})
				case lifecycle.CrossOff:
					onStop(glctx)
					glctx = nil
				}
			case size.Event:
				sz = e
				touchX = float32(sz.WidthPx / 2)
				touchY = float32(sz.HeightPx / 2)
			case paint.Event:
				if glctx == nil || e.External {
					// As we are actively painting as fast as
					// we can (usually 60 FPS), skip any paint
					// events sent by the system.
					continue
				}

				onPaint(glctx, sz, cells)
				a.Publish()
				// Drive the animation by preparing to paint the next frame
				// after this one is shown.
				a.Send(paint.Event{})
			case touch.Event:
				touchX = e.X
				touchY = e.Y
			}
		}
	})
}

func onStart(glctx gl.Context) {
	var err error
	program, err = glutil.CreateProgram(glctx, vertexShader, fragmentShader)
	if err != nil {
		log.Printf("error creating GL program: %v", err)
		return
	}

	// find the position and color named fields inside shaders
	position = glctx.GetAttribLocation(program, "position")
	color = glctx.GetUniformLocation(program, "color")

	images = glutil.NewImages(glctx)
	fps = debug.NewFPS(images)
}

func onStop(glctx gl.Context) {
	glctx.DeleteProgram(program)
	for x := range cells {
		for _, c := range cells[x] {
			glctx.DeleteBuffer(c.drawable)
		}
	}
	fps.Release()
	images.Release()
}

func onPaint(glctx gl.Context, sz size.Event, cells [][]*cell) {
	// choose bkgrnd color
	glctx.ClearColor(0, 0, 0, 1)
	glctx.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	glctx.UseProgram(program)

	for x := range cells {
		for _, c := range cells[x] {
			c.checkState(cells)
			c.colorAge()
			c.draw(glctx)
		}
	}

	fps.Draw(sz)
}

func (c *cell) draw(glctx gl.Context) {
	if !c.alive {
		return
	}
	glctx.Uniform4f(color, c.rval, c.gval, c.bval, 1)

	glctx.BindBuffer(gl.ARRAY_BUFFER, c.drawable)
	glctx.EnableVertexAttribArray(position)
	glctx.VertexAttribPointer(position, coordsPerVertex, gl.FLOAT, false, 0, 0)
	glctx.DrawArrays(gl.TRIANGLES, 0, vertexCount)
	glctx.DisableVertexAttribArray(position)
}

// checkState determines the state of the cell for the next tick of the game
func (c *cell) checkState(cells [][]*cell) {
	c.alive = c.aliveNext
	c.aliveNext = c.alive

	liveCount := c.liveNeighbors(cells)
	if c.alive {
		// 1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
		if liveCount < 2 {
			c.aliveNext = false
		}

		// 2. Any live cell with two or three live neighbours lives on to the next generation.
		if liveCount == 2 || liveCount == 3 {
			c.aliveNext = true
		}

		// 3. Any live cell with more than three live neighbours dies, as if by overpopulation.
		if liveCount > 3 {
			c.aliveNext = false
		}
	} else {
		// 4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
		if liveCount == 3 {
			c.aliveNext = true
		}
	}
}
func (c *cell) liveNeighbors(cells [][]*cell) int {
	var liveCount int
	add := func(x, y int) {
		// If we're at an edge, check the other side of the board.
		if x == len(cells) {
			x = 0
		} else if x == -1 {
			x = len(cells) - 1
		}
		if y == len(cells[x]) {
			y = 0
		} else if y == -1 {
			y = len(cells[x]) - 1
		}

		if cells[x][y].alive {
			liveCount++
		}
	}

	add(c.x-1, c.y)   // To the left
	add(c.x+1, c.y)   // To the right
	add(c.x, c.y+1)   // up
	add(c.x, c.y-1)   // down
	add(c.x-1, c.y+1) // top-left
	add(c.x+1, c.y+1) // top-right
	add(c.x-1, c.y-1) // bottom-left
	add(c.x+1, c.y-1) // bottom-right

	return liveCount
}

func (c *cell) colorAge() {
	if !c.alive {
		c.aliveFrames = 0
	} else {
		c.aliveFrames += 1
	}

	c.rval = 0
	c.gval = 0.51
	c.bval = 0
	if c.aliveFrames > 20 {
		c.rval = 1
		c.gval = 0
		c.bval = 1
	} else if c.aliveFrames > 3 {
		c.rval = 1
		c.gval = 1
		c.bval = 0
	}
}

func makeCells(glctx gl.Context) [][]*cell {
	rand.Seed(time.Now().UnixNano())

	cells := make([][]*cell, rows, rows)
	for x := 0; x < rows; x++ {
		for y := 0; y < columns; y++ {
			c := newCell(glctx, x, y)

			// start cell at 15% chance to be live
			c.alive = rand.Float64() < threshold
			c.aliveNext = c.alive

			cells[x] = append(cells[x], c)
		}
	}
	return cells
}

// resizes or scales the square vertex data to fit the grid dimensions
// and then position per x,y of the grid
func newCell(glctx gl.Context, x, y int) *cell {
	points := make([]float32, len(square), len(square))
	copy(points, square)
	for i := 0; i < len(points); i++ {
		var position float32
		var size float32
		switch i % 3 {
		case 0:
			size = 1.0 / float32(columns)
			position = float32(x) * size
		case 1:
			size = 1.0 / float32(rows)
			position = float32(y) * size
		default:
			// no z-component
			continue
		}
		if points[i] < 0 {
			points[i] = (position * 2) - 1
		} else {
			points[i] = ((position + size) * 2) - 1
		}
	}

	data := f32.Bytes(binary.LittleEndian, points...)
	return &cell{
		drawable: makeVao(glctx, data),
		x:        x,
		y:        y,
	}
}

// see kylewbanks.com/blog/tutorial-opengl-with-golang-part-1-hello-opengl
func makeVao(glctx gl.Context, data []byte) gl.Buffer {
	var vbo gl.Buffer
	vbo = glctx.CreateBuffer()
	glctx.BindBuffer(gl.ARRAY_BUFFER, vbo)
	glctx.BufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW)
	return vbo
}

var square = []float32{
	-0.5, 0.5, 0.0, // tri top
	-0.5, -0.5, 0.0, // tri bottom lt
	0.5, -0.5, 0.0, // tri bottom rt

	-0.5, 0.5, 0.0, // tri top lt
	0.5, 0.5, 0.0, // tri top rt
	0.5, -0.5, 0.0, // tri bottom
}

const (
	coordsPerVertex = 3
	vertexCount     = 6
	rows            = 40
	columns         = 40
	threshold       = 0.15
)

const vertexShader = `
	#version 100
	attribute vec4 position;
	void main() {
		gl_Position = position;
}`

const fragmentShader = `
	#version 100
	precision mediump float;
	uniform vec4 color;
	void main() {
		gl_FragColor = color;
}`
