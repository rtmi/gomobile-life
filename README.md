# android-life
This is a Android mobile version of the Game-of-Life from the 
 [Kyle Banks tutorial.](https://github.com/KyleBanks/conways-gol/)
 Here, the Gomobile toolchain takes the place of the GLFW and OpenGL calls. 

![Image of Android app](img/android-life.png)

## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [src](https://github.com/patterns/life/)
3. From the cloned directory, build and get the .APK file


```console
$ docker build -t life .
$ docker run -it --rm -v $(pwd):/go/app/life life
# cp app.apk /go/app/life/life.apk
# exit
$ adb install life.apk
```


## Credits

Game of Life is from
 [Kyle Banks](https://github.com/KyleBanks/conways-gol/)
 [(LICENSE)](https://github.com/KyleBanks/conways-gol/blob/master/LICENSE)

Gomobile is by
 [The Go Authors](https://github.com/golang/mobile/)
 [(LICENSE)](https://github.com/golang/mobile/blob/master/LICENSE)
